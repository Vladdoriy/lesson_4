// #1
enum NameOfRelatives:String {
    case mum = "Natasha"
    case dad = "Oleg"
    case grandma = "Luda"
    case granpa = "Kolya"
}
   
enum YearOfFounding:Int {
    case vitebsk = 974
    case piter = 1703
    case minsk = 1067
    case moskva = 1147
}

// #2
enum Sex {
    case man
    case women
}

enum AgeCategory {
    case child
    case teenager
    case man
    case oldMan
}

enum Experience {
    case student
    case journeyman
    case master
    case head
}

struct Workman {
    let sex: Sex
    let ageCategory: AgeCategory
    let experience: Experience
    }

let man = Workman(sex: .man, ageCategory: .oldMan, experience: .master)

//#3
enum RainbowColours {
    case red
    case orange
    case yellow
    case green
    case blue
    case darkBlue
    case purple
}

//#4
func CollourOfObjects(){
    var arrayOfColours = [
    RainbowColours.darkBlue, RainbowColours.green,
    RainbowColours.red, RainbowColours.blue
   ]
    
    print("""
    Car is \(arrayOfColours[0])
    Grass is \(arrayOfColours[1])
    Apple is \(arrayOfColours[2])
    Sky is \(arrayOfColours[3])
    """)
}
CollourOfObjects()

//#5
enum Score: String {
    case zero = "0"
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
}
func mark (mark: Score){
    print(Int(mark.rawValue)!)
}
print(mark(mark: Score.four))

//#6
enum Cars: String {
    case car = "bmw"
    case car2 = "mercedes"
    case car3 = "audi"
    case car4 = "golf"
}

func printOfGarage (printOfGarage: Cars) {
    print(String(printOfGarage.rawValue))
}
print(printOfGarage(printOfGarage:Cars.car3))

